$(document).ready(function() {
    var $menuHamburger = $('.c-mobile-menu__hamburger');
    var $mobileSubmenu = $('.c-mobile-submenu');

    // Update container
    updateContainer();

    $menuHamburger.on( 'click', function(e) {
        $mobileSubmenu.slideToggle( 'slow' );
    });
  
    // Create Owl carousel
    $('.owl-carousel').owlCarousel({
        loop:true,
        //items: 2,
        dots: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        nav: true,
        navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
        margin: 0,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            768:{
                items:2,
                nav:true
            },
        }
    });
});

// Handle resize events
$(window).resize(function() {
    updateContainer();
});

function updateContainer() {
    var $navtopBase = $('.c-page-header__nav');
    var $navtopMobile = $('.c-page-header__nav-mobile');
   
    if ($(window).width() < 900) {
        $navtopBase.removeClass('is-visible');
        $navtopMobile.addClass('is-visible');

    } else {
        $navtopBase.addClass('is-visible');
        $navtopMobile.removeClass('is-visible');
    }
}




